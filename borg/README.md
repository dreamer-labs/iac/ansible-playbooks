## Required Variables

These variables need to be set and exported
`export borg_repo_ssh_priv_key`
`export borg_repo_ssh_pub_key`
`export borg_pass`

To run in OVH:

```bash
export OS_FORCE_IPV4=true
# source openrc file from OVH
ansible-playbook provision-borg.yml
ansible-playbook -i ../inventories/openstack_inventory.py deploy-borg-server.yml
ansible-playbook -i ../inventories/openstack_inventory.py deploy-borg-client.yml
```

To run elsewhere you must create your servers, define your inventory, and then
run the deploy-borg-server and deploy-borg-client playbooks
